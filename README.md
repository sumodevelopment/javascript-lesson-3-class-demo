# JavaScript Class demo - Lesson 3

The lesson includes the usage of JS Classes, Template literals, Arrow functions, destructering and modules.

## The Car Factory
Use a form to build custom cars and add it to the shop inventory. 

## Live Demo
Find a live demo here: [https://sumodevelopment.gitlab.io/javascript-lesson-3-class-demo/](https://sumodevelopment.gitlab.io/javascript-lesson-3-class-demo/)

## Dependencies
None

## Contributors
Dewald Els

