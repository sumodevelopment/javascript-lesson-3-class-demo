class Car {
    constructor(model, colour, seats = 4) {
        this.model = model
        this.colour = colour
        this.seats = seats
    }
}

class CarFactory {

    constructor() {
        this.elCarFactoryStatus = document.getElementById('car-factory-status')
    }

    create(model, colour, seats) {
        const car = new Car(model, colour, seats)
        return car
    }

    setStatus(text) {
        this.elCarFactoryStatus.innerText = text
    }

}

class CarShop {

    constructor(cars = []) {
        this.cars = cars
        this.elCarList = document.getElementById('cars') 
    }

    addToInventory(car) {
        this.cars.push(car)
    }

    clearDisplay() {
        this.elCarList.innerHTML = ''
    }

    display() {
        this.clearDisplay()
        for (const car of this.cars) {
            this.elCarList.innerHTML += `<li>${car.model} - ${car.colour} with ${car.seats} seats.</li>`
        }
    }
}

const carFactory = new CarFactory()
const carShop = new CarShop([])

const elCarFactoryForm = document.getElementById('car-factory-form')

elCarFactoryForm.addEventListener('submit', onCarFactorySubmit)

async function onCarFactorySubmit(event) {
    event.preventDefault()
    
    const formData = new FormData(elCarFactoryForm)
    
    const car = carFactory.create(
        formData.get('model'),
        formData.get('colour'),
        formData.get('seats')
    )
    
    carFactory.setStatus('Creating ' + car.model +  ' car...')
    await wait(2000)
    carFactory.setStatus('Painting your car ' + car.colour + '...')
    await wait(2000)
    carFactory.setStatus('Fitting ' + car.seats + ' seats...')
    await wait(2000)

    carFactory.setStatus('Created your new car, adding to available cars')
    await wait(2000) 
    
    carShop.addToInventory(car)
    carShop.display()


    carFactory.setStatus('Ready to start')
}

function wait(duration) {
    return new Promise((resolve) => {
        setTimeout(resolve, duration)
    })
}