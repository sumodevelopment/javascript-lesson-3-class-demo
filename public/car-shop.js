class CarShop {

    constructor() {
        this.inventory = document.getElementById('shop-inventory')
        this.cars = []
    }

    addInventory(car) {
        this.cars.push(car)
    }

    display() {

        this.clearDisplay()

        for (const car of this.cars) {
            this.inventory.innerHTML += 
                `<li>${car.model} - ${car.colour} with ${car.seats} seats.</li>`
        }
    }

    clearDisplay() {
        this.inventory.innerHTML = ''
    }
}

export default CarShop