import CarFactory from './car-factory.js'
import CarShop from './car-shop.js'
import { sleep } from './utils.js'

const carFactory = new CarFactory()
const carShop = new CarShop()

const carFactoryForm = document.getElementById('car-factory-form')
carFactoryForm.addEventListener('submit', onFactoryCreate)

async function onFactoryCreate(event) {
    event.preventDefault()
    const factoryData = new FormData(carFactoryForm)
    const carData = {
        model: factoryData.get('model'),
        colour: factoryData.get('colour'),
        seats: factoryData.get('seats')
    }
    
    carFactory.setStatus('Creating your car...')
    await sleep(2000)
    const car = carFactory.create(carData)
    
    carFactory.setStatus('Preparing your ' + car.model + '...')
    await sleep(2000)

    carFactory.setStatus('Painting it ' + car.colour)
    await sleep(2000)

    carFactory.setStatus('Fitting ' + car.seats + ' seats...')
    await sleep(2000)

    carShop.addInventory(car)
    carFactory.setStatus('Adding the ' + car.model + ' to the inventory...')
    await sleep(2000)

    carShop.display()
    
    carFactory.setStatus('Ready') 
    
    carFactoryForm.reset()
}



