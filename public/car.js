class Car {
    constructor(model, colour, seats) {
        this.model = model
        this.colour = colour
        this.seats = seats
    }
}

export default Car