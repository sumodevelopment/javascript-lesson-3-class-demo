import Car from './car.js'

class CarFactory {
    constructor() {
        this.factoryStatus = document.getElementById('car-factory-status')
    }

    create({ model, colour, seats }) {
        return new Car(model, colour, seats)
    }

    setStatus(text) {
        this.factoryStatus.innerText = text
    }
    
}

export default CarFactory
